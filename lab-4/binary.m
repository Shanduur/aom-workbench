% Group 2:
% - Chowańska Katarzyna
% - Porębski Artur
% - Urbanek Mateusz
clear all; close all; clc;

% TASK 2 (set 2)
% max <- v = Sum[i = 1:N](Xi * Vi) 
% Sum[i = 1:N](Xi * Wi) <= Wmax
% Xi = [{0, 1}, i = 1:N]
Wmax = 17.5;
Vi = [ 15, 23,   5,   6,  15,   11,  25, 2.3];
Wi = [3.2,  4, 1.2, 2.1, 2.8,  3.3, 4.2, 0.8];
N = length(Wi);

% intlinprog