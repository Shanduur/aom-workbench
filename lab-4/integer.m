% Group 2:
% - Chowańska Katarzyna
% - Porębski Artur
% - Urbanek Mateusz
clear all; close all; clc;

% TASK 1 (set 2)
% max C^T * x
% Ax <= b
% x >= 0, x € Z
A = [ 
        -1.42269 1;
        0.837579 1;
        1.06244  1;
    ]
b = [ 2.7519; 3.1003; 6.245 ]

cT = [3.1003; 5.878]

% linprog
[x, fval] = linprog(-cT, A, b, [], [], [0 0], [inf inf]);

disp(x)
disp(fval)