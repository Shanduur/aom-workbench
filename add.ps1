git add .

Switch ($args[0]) {
    "lab" {
        git commit -m "feat(lab): added labs code from $(Get-Date -Format "yyyy/MM/dd")"
    }
    "lect" {
        git commit -m "feat(lect): added lecture code from $(Get-Date -Format "yyyy/MM/dd")"
    }
} 

git push
