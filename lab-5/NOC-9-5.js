let lifetime;
let population;
let lifecycle;
let recordtime;
let target;
let obstacles = [];
let results_1 = [];
let results_2 = [];
let mutationRate;
let populationSize;

function setup() {
  createCanvas(640, 360);
  lifetime = 350;
  lifecycle = 0;
  recordtime = lifetime;
  target = new Obstacle(width / 2 - 12, 24, 24, 24);
  mutationRate = 0.01;
  populationSize = 50;
  population = new Population(mutationRate, populationSize);
  obstacles = [];
  obstacles.push(new Obstacle(width / 2 - 100, height / 2 + 50, 200, 10));
  obstacles.push(new Obstacle(width / 4 + 120, height / 3, 200, 10));
}

function draw() {
  background(127);
  target.display();

  if (lifecycle < lifetime) {
    population.live(obstacles);
    if (population.targetReached() && lifecycle < recordtime) {
      recordtime = lifecycle;
      results_1.push(population.getGenerations());
      results_2.push(lifecycle);
      
      let tmp = "";
      for (let i = 0; i < results_1.length; i++) {
        tmp += "" + mutation rate + "," +  + "," + results_1[i] + "," + results_2[i] + "|";
      }
      console.log(tmp);
    }
    lifecycle++;
  } else {
    lifecycle = 0;
    population.calcFitness();
    population.selection();
    population.reproduction();
  }

  for (let i = 0; i < obstacles.length; i++) {
    obstacles[i].display();
  }

  fill(0);
  noStroke();
  text("Generation #: " + population.getGenerations(), 10, 18);
  text("Cycles left: " + (lifetime - lifecycle), 10, 36);
  text("Record cycles: " + recordtime, 10, 54);
}