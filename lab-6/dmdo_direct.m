clear all

%% Init
N = 5;
A = 1;
B = 1.5;
Q = 1;
R = 0.5;
T = 2.5;

K = 30;
eps = 0.25;

u_arr = zeros(K, N);
u_arr(1, :) = [0.1, 0.4, 0.3, 0.5, 0.2];

x_arr = zeros(K, N + 1);
x_arr(:, 1) = 0.6;

p = zeros(1, N + 1);
b = zeros(1, N);
F = zeros(K, 1);
t = 0.01;

%% Algorithm
for iter = 1 : K
    for i = 2 : size(x_arr, 2)
        x_arr(iter, i) = A * x_arr(iter, i - 1) ^ 2 + B * u_arr(iter, i - 1);
    end
    p(end) = 2 * T * x_arr(end) ^ 3;
    for i = N : -1 : 1
        p(i) = 2 * x_arr(iter,i) * (Q + p(i + 1) * A);
    end
    for i = 1 : length(b)
        b(i) = 2 * R * u_arr(iter, i) + B * p(i + 1);
    end
    F(iter) = sum(Q * x_arr(iter, 1 : end - 1) .^ 2 + R * u_arr(iter, :) .^ 2) + T / 2 * x_arr(end) ^ 4;   
    if norm(b) < eps
        break;
    end
    u_arr(iter + 1, :) = u_arr(iter, :) - t * b;
end

%% Plot
colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k'];
figure;
subplot(1, 2, 1);
for i = 2 : N + 1
    plot(x_arr(1 : iter, i), colors(i - 1));
    hold on;
end
title('Trajectories, direct gradient');
hold off;

subplot(1, 2, 2);
plot(F(1 : iter));
title('Performance index, direct gradient');