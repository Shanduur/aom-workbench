"""
Advanced Optimization Methods
Lab 1: Decision Trees

Group 6:
- Chowańska Katarzyna
- Porębski Artur
- Potempa Rafał
- Urbanek Mateusz
"""

import numpy as np
import sys
import json


game_2x2 = np.array([
    [-5.,  6.],
    [14., -7.]
])

game_2x3 = np.array([
    [ 4., -5., -8.],
    [-9.,  6.,  9.]
])

game_mxn = np.array([
    [ 15., -13.,  10.,  11., -17.],
    [ 13.,  12., -11., -11.,  16.],
    [-12.,  10.,  14.,  14., -13.],
    [ 14.,  18., -15., -15.,  11.],
    [ 17., -19.,  11.,  16., -12.]
])

game_non_nash = np.array([
    [[ -4., -4.], [ 0., -10.]],
    [[-10.,  0.], [-1.,  -1.]]
])

game_nash = np.array([
    [[ 0.,  0.], [25., 40.], [ 5., 10.]],
    [[40., 25.], [ 0.,  0.], [ 5., 15.]],
    [[10.,  5.], [15.,  5.], [10., 10.]],
])


def read_game(path: str) -> np.ndarray:
    f = open(path)
    data = json.load(f)

    return np.array(data['GAME'])


def is_zero_sum(game: np.ndarray) -> bool:
    return len(game.shape) == 2


def generate_random(cols: int, rows: int, low=-99, high=99) -> np.array:
    return np.random.uniform(low=low, high=high, size=(cols,rows))


def solve(game: np.ndarray) -> list:
    if is_zero_sum(game):
        return min_max(game)
    else:
        if (result := nash(game)) != []: return result
        else: return safe(game)


def safe(game: np.ndarray) -> list:
    p0 = np.argmax(np.min(game[:,:,0], axis=0))
    p1 = np.argmax(np.min(game[:,:,1], axis=1))

    return [{'row': p0, 'col': p1, 'value': game[p0, p1]}]


def nash(game: np.ndarray) -> list:
    col_max = game[:,:,0] == np.max(game[:,:,0], axis=0)
    row_max = (game[:,:,1].T == np.max(game[:,:,1], axis=1)).T
    values = game[col_max & row_max]
    idx = np.where(col_max&row_max == True)

    result = [{'row': r, 'col': c, 'value': v} for r, c, v in zip(*idx, values)]
    return result


def min_max(game: np.ndarray) -> list:
    out = {'row':0, 'col':0, 'value':-sys.maxsize}

    arr = np.full(shape=game.shape[0], fill_value=sys.maxsize, dtype=float)
    idx = np.full(shape=game.shape[0], fill_value=0, dtype=int)

    # find minimum for each row
    for i, _ in enumerate(game):
        for j, _ in enumerate(game[i]):
            if arr[i] > game[i, j]:
                arr[i] = game[i, j]
                idx[i] = j

    # find maximum from minima
    for y, val in enumerate(arr):
        if out['value'] < val:
            out['value'] = val
            out['col'] = idx[y]
            out['row'] = y

    return [out]


def max_min(game: np.ndarray) -> list:
    out = {'row':0, 'col':0, 'value':sys.maxsize}

    arr = np.full(shape=game.shape[0], fill_value=-sys.maxsize, dtype=float)
    idx = np.zeros(shape=game.shape[0], fill_value=0, dtype=int)

    # find maximum for each row
    for i, _ in enumerate(game):
        for j, _ in enumerate(game[i]):
            if arr[i] < game[i, j]:
                arr[i] = game[i, j]
                idx[i] = j

    # find minimum from maximums
    for y, val in enumerate(arr):
        if out['value'] > val:
            out['value'] = val
            out['col'] = idx[y]
            out['row'] = y

    return [out]


def main():
    matrix = [game_2x2, game_2x3, game_mxn, game_nash, game_non_nash]

    gen = generate_random(3,5)

    print(gen)
    matrix.append(gen)

    for game in matrix:
        print(solve(game))

    pass


if __name__ == '__main__':
    main()
