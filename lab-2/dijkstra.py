# %%
import jstyleson as json
import pandas as pd
import numpy as np
import sys
# %%
def read_json(path: str) -> np.ndarray:
	f = open(path)
	data = json.load(f)

	adjacency_matrix = np.array(data['adjacency_matrix']).astype(float)
	adjacency_matrix[adjacency_matrix == 0.0] = np.inf
	np.fill_diagonal(adjacency_matrix, 0)

	return adjacency_matrix

if __name__ == '__main__':
	read_json('test.jsonc')
# %%
def dijkstra(adjacency_matrix: np.ndarray, initial_node: int, target_node: int) -> tuple:
	n = adjacency_matrix.shape[0]

	paths = pd.DataFrame(index=range(n))
	paths['p'] = initial_node
	paths['d'] = adjacency_matrix[initial_node]

	nodes = np.argsort(adjacency_matrix[initial_node])[1:]
	
	for v0 in nodes:
		for v1 in np.argsort(adjacency_matrix[v0])[[d not in [0, np.inf] for d in np.sort(adjacency_matrix[v0])]]:
			d = paths.d[v0] + adjacency_matrix[v0, v1]
			if d < paths.d[v1]:
				paths.loc[v1, 'p'] = v0
				paths.loc[v1, 'd'] = d

	# recreate shortest path
	path = []
	node = target_node
	while True:
		path.append(node)
		node = paths.p[node]
		if node == initial_node:
			path.append(initial_node)
			break
	path.reverse()

	return {'path': path, 'distance': paths.d[target_node]}

if __name__ == '__main__':
	dijkstra(read_json('test.jsonc'), 1, 4)
