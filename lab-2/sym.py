import jstyleson as json
import numpy as np
import sys

def read_json(path: str) -> np.ndarray:
    f = open(path)
    data = json.load(f)

    return np.array(data['adjacency_matrix'])


def check_symmetric(a):
    for i in range(a.shape[0]):
        for j in range(a.shape[1]):
            if a[i, j] != a.T[i, j]:
                print(f'[{i+1}, {j+1}]: {a[i,j]}')


def main():
    adjacency_matrix = read_json('graph_task.jsonc')

    print(f'shape: {adjacency_matrix.shape}')
    check_symmetric(adjacency_matrix)

    pass


if __name__ == '__main__':
    main()