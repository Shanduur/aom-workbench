"""
Advanced Optimization Methods
Lab 2: Graph Optimization Problems

Group 6:
- Chowańska Katarzyna
- Porębski Artur
- Potempa Rafał
- Urbanek Mateusz

Task:
    Select one of those:
    - Dijkstra algorithm
    - Bellman-Ford algorithm.

    The obligatory input data to your program are: 
    1) Weighted graph structure, defined by ADJACENCY MATRIX G,
    2) Initial node: S,
    3) Target node: T.

    The result of the program operation must be both:
    - Least cost path defined by a sequence of transitions between nodes starting from S to T,
    - The value of the least cost.

    Procedure:
    1) Select 15 towns from the map and label them as the graph vertices.
    2) Using any map available on-line determine graph arcs defining possible connections between selected towns.
    3) Using the same on-line map find shortest distances di,j between towns selected. Attribute these distances
        to the weighting factors wi,j of the arcs (where i and j denotes indices of two vertices connected)
        which define the costs of transition between vertices i and j.

    Using such data create the adjacency matrix G of size 15x15 and use it as the input data to your program
    solving the shortest path problem. The result of executing Your program is going to be the shortest distance
    between vs and vT and the path, sequence of vertices that must be visited to complete the route.
"""
from dijkstra import *

cities = [
    'Wroclaw',
    'Bydgoszcz',
    'Lublin',
    'Lodz',
    'Krakow',
    'Warszawa',
    'Opole',
    'Rzeszow',
    'Bialystok',
    'Gdansk',
    'Katowice',
    'Kielce',
    'Olsztyn',
    'Poznan',
    'Szczecin',
]

def main():
    adjacency_matrix = read_json('task.jsonc')
    initial_city = 'Szczecin'
    target_city = 'Rzeszow'

    result = dijkstra(adjacency_matrix, cities.index(initial_city), cities.index(target_city))
    print(f"Route: {initial_city} -> {target_city}")
    print(f"Dijkstra algorithm:\n- path: {' -> '.join([cities[i] for i in result['path']])}\n- distance: {result['distance']} km")

if __name__ == '__main__':
    main()
